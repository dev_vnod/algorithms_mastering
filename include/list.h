#ifndef LIST_H
#define LIST_H

typedef struct ListElem_{
    void *data;
    struct ListElem_ *next;
}ListElem;

typedef struct List_{
    ListElem *head;
    ListElem *tail;
    int size;

    /* A function pointer to free() */
    void (*destroy) (void *data);

    /* A function pointer to match the data */
    int (*match) (const void *data1, const void *data2);
}List;


/* Initialize the list */
void list_init(List *list, void (*destroy) (void *data));

/* Destroy the list */
void destroy_list (List *list);


/* Insert the new listElem
 *  At HEAD: if second parameter  is NULL
 * AT TAIL: if second parameter is not NULL
 */
int  list_insert_next(List *list, ListElem *element, const void *data);

/*  Remove the element from the list */
int list_remove_next(List *list, ListElem *element, void **data);

#define list_size(list)     (list->size)
#define list_head(list)     (list->head)
#define list_tail(list)     (list->tail)

#define list_is_head(list, element)     (list_head(list) == element ? 1:0 )
#define list_is_tail(element)           (element->next == NULL ? 1:0 )
#define list_data(element)              (element->data)
#define lis_next(element)               (element->next)
#define list_size(list)                 (list->size)




#endif
